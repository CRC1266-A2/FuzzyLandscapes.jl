# This file contains a helper funtion to calculate the euclidean distance

"""
# Calculate the euclidean distance from an index point

    input: your input data, for example your location of tif file "../slope.tif"

    x: matrix index of the point, from which distances are calculated, along x axis

    y: matrix index of the point, from  which distances are calculated, along y axis

"""

function calculate_euclidean_distance(input::String, x::Int, y::Int)
    coords_matrix = collect(GeoArrays.coords(GeoArrays.read(input)))
    dist_matrix = fill(0.00, size(coords_matrix))

    for i in 1:size(coords_matrix)[1]
        for j in 1:size(coords_matrix)[2]
            dist_matrix[i, j] = Distances.Euclidean()(coords_matrix[i, j],
                                                      coords_matrix[x, y])
        end
    end

    return dist_matrix
end

"""
# Calculate the euclidean distance from a coordinate

    input: your input data, for example your location of tif file "../slope.tif"
    
    xcor: x coordinate of the point, from which distances are calculated, along x axis
    
    ycor: x coordinate of the point, from  which distances are calculated, along y axis

"""

function calculate_euclidean_distance(input::String, xcor::Float64, ycor::Float64)

    coords_matrix = collect(GeoArrays.coords(GeoArrays.read(input)))

    dist_matrix = fill(0.00, size(coords_matrix))

    x_ind = GeoArrays.indices(data, [xcor, ycor])[1]

    y_ind = GeoArrays.indices(data, [xcor, ycor])[2]

    for i in 1:size(coords_matrix)[1]
        for j in 1:size(coords_matrix)[2]
            dist_matrix[i, j] = Distances.Euclidean()(coords_matrix[i, j],
                                                      coords_matrix[x_ind, y_ind])
        end
    end

    return dist_matrix
end

export calculate_euclidean_distance
