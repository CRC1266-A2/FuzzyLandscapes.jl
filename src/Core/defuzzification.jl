
### add NoData_input::Any = nothing, NoData_output::Any = nothing to all functions

"""
# Defuzzification

## Input Raster to fuzzy raster without export

    input - your input data,
            for example your location of tif file "../example.tif"

    method - choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1,x2,x3,x4 - parameters of membership function
                  for further information look into membership_functions.jl

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    NoData_input: Define a 'NoData_input' value, if you have a numeric 'NoData' in your input data.

    NoData_output: Define a 'NoData_output' value to set the NoData value in the fuzzified set. 

"""

function defuzzification(input::String, method::Symbol, defuzzy_method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; NoData_input::Any = nothing, NoData_output::Any = nothing)

    input_matrix, input_raster = FuzzyLandscapes.raster_to_array(input)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))
    
    fuzzified_matrix = FuzzyLandscapes.fuzzification(input_matrix, fuzzy_matrix, method, x1,
                                                    x2, x3, x4; NoData_input, NoData_output)

    defuzzy_output = defuzzy(input_matrix, fuzzified_matrix, defuzzy_method; NoData_input)

    if !isempty(fuzzified_matrix)

        fuzzified_matrix_to_raster = FuzzyLandscapes.array_to_raster(fuzzified_matrix,
                                                                    input_raster; NoData_input, NoData_output)

        return fuzzified_matrix_to_raster, defuzzy_output

        @info "Defuzzification successful."

    else
        @error "Defuzzification unsuccessful. Possible reasons are types in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

"""
# Defuzzification

## Input matrix to fuzzy matrix without export

    input - your input data,
            for example your location of tif file "../example.tif"

    method - choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1,x2,x3,x4 - parameters of membership function
                  for further information look into membership_functions.jl

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    NoData_input: Define a 'NoData_input' value, if you have a numeric 'NoData' in your input data.

    NoData_output: Define a 'NoData_output' value to set the NoData value in the fuzzified set. 
"""

function defuzzification(input::AbstractMatrix{<:Real}, method::Symbol, defuzzy_method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; NoData_input::Any = nothing, NoData_output::Any = nothing)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))

    fuzzified_matrix = FuzzyLandscapes.fuzzification(input, fuzzy_matrix, method, x1, x2, x3,
                                                    x4; NoData_input, NoData_output)

    defuzzy_output = defuzzy(input, fuzzified_matrix, defuzzy_method; NoData_input)

    if !isempty(fuzzified_matrix)

        return fuzzified_matrix, defuzzy_output

        @info "Defuzzification of matrix successful."
    else
        @error "Defuzzification of matrix unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

export defuzzification

"""
# Defuzzification

## This function is part of the 'defuzzification()'-function

    input: your input data, for example your location of tif file "../example.tif"

    fuzzified_matrix: A fuzzified matrix

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    NoData_input: Define a 'NoData_input' value, if you have a numeric 'NoData' in your input data.

    NoData_output: Define a 'NoData_output' value to set the NoData value in the fuzzified set. 
"""

function defuzzy(input::AbstractMatrix{<:Real}, fuzzified_matrix::AbstractMatrix{<:Real}, defuzzy_method::Symbol; NoData_input::Any = nothing)
    
    if defuzzy_method == :Centroid
        defuzzy_output = FuzzyLandscapes.centroid(input, fuzzified_matrix; NoData_input)
    elseif defuzzy_method == :Mom
        defuzzy_output = FuzzyLandscapes.mom(input, fuzzified_matrix)
    elseif defuzzy_method == :Som
        defuzzy_output = FuzzyLandscapes.som(input, fuzzified_matrix)
    elseif defuzzy_method == :Lom
        defuzzy_output = FuzzyLandscapes.lom(input, fuzzified_matrix)
    elseif defuzzy_method == :Bisector
        defuzzy_output = FuzzyLandscapes.bisector(input, fuzzified_matrix; NoData_input)
    end

    return defuzzy_output
end
