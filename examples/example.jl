### A Pluto.jl notebook ###
# v0.19.36

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ fe7d2fc8-0879-432e-b937-0e7c30c30ec6
begin
	import Pkg
    using ArchGDAL
    using Plots
    using PlutoUI
	using PlutoTeachingTools
end

# ╔═╡ cea81956-b776-4234-a0c6-ea7c8605a282
Pkg.add(url = "https://gitlab.com/CRC1266-A2/FuzzyLandscapes.jl", rev = "master")

# ╔═╡ a9e63777-07c5-439e-a210-5333bfb6dbc9
using FuzzyLandscapes

# ╔═╡ 768f6811-3795-434e-9ae1-e72e589b585a
TableOfContents()

# ╔═╡ f04b7695-7213-4379-b910-262516932041
ChooseDisplayMode()

# ╔═╡ 3d290f8e-88d5-4f12-877b-eba0533e6c9b
md"

# Introduction

Fuzzy logic, introduced by Zadeh (1965), is a method to classify data based on linguistic variables that help to define classes whose limits are uncertain. The general idea is to define a degree of belonging to a particular class rather than setting crisp values for it.

A common example to illustrate this concept is the driving speed of a car. Anyone can imagine a fast moving car, but you can't just tell a model that the speed of a car is fast. Accordingly, using the fuzzy approach, a function is defined that maps a value between 0, i.e. the value does not belong to the fuzzy class, and 1, i.e. the value belongs completely to the fuzzy class, depending on the speed. This is shown exemplarily in the plot below. A speed, which according to the function achieves a high value, in the plot e.g. 80 km/h, would have a high affiliation to the fuzzy class, which describes this function. In our example this fuzzy class is the expression 'high speed'.

"

# ╔═╡ 81943deb-eda1-4ef8-8b32-ab79f0a30edf
md"""

## Add the package

For this example, we add the package using the following lines of code

"""

# ╔═╡ 65d7ac9c-b205-4723-942e-6f2d50b69a5e
md"""
If you use fuzzylandscapes for your project, you currently have to clone the repository and add
"""

# ╔═╡ 72a240f5-63a4-415a-be33-6f7c42893935
md"

## Basics

Using FuzzyLandscapes.jl, we can assign a membership degree to a particular class rather than setting crisp values by using membership functions. In the following sections, we make use of the interactivity of Pluto.jl to illustrate the capabilities of FuzzyLandscapes.jl. You can use the sliders to define boundaries of the membership degrees and at look at the effects of different input parameters.


"

# ╔═╡ 30b4988c-5fbc-4af4-bd82-36a38c052ca0
begin

md"""

Using the 'frbs_classification' we are able to fuzzify the crisp boundaries of our data. The function can use differetent input parameters for that task.

##### Overview of frbs_classification function:

- Class: The name of your class, which you want to derive with this function, for example "Flat", "Moist", "Far", etc.

- Input: The input data, for example your location of tif file "../example.tif"

- Method: The membership funtione you want to use. Currently, there are several membership functions to choose from: 

- :Generalised_bell
- :Trapezoidal
- :Triangular
- :Gaussian
- :Gaussian2
- :Sigmoid
- :D_Sigmoid
- :Z
- :S
- :PI

And finally, you have to define the limits, boundaries and/or breaking points of the membership function: 

- x1, x2, x3, x4

Different numbers of input parameters are required for each function. For further information, please have a look into membership_functions.jl and for more information about the membership functions see [this](https://de.mathworks.com/help/releases/R2017a/fuzzy/).

""" end

# ╔═╡ 7532bb6a-bc01-43ac-a9a7-9577515ef114
md"

For our example, we use the 

- :Trapeziodial 
- :Triangular
- :Z

"

# ╔═╡ 2294d23b-85b4-4920-be4f-7a0deb110e39
md"
**Trapezoidal**

Using the Trapeziodial membership funtion, we can e.g. define the suitability (membership) of an area, based on the distance. In this context, we can think of a land-use practice, which can only be done in a distance between 2 and 6 km distance. Thus, the membership degree or suitability increases from 2 km to 3 km. From this point, the membership settles until 5 km distance, from which the suitability decreases, until dropping to 0 in 6 km distance.

"

# ╔═╡ ad09e1af-6806-496e-b4ba-beb570adac7c
begin
    x1 = @bind x1 Slider(0:0.1:10, default = 2)
    x2 = @bind x2 Slider(0:0.1:10, default = 3)
    x3 = @bind x3 Slider(0:0.1:10, default = 5)
    x4 = @bind x4 Slider(0:0.1:10, default = 6)

    md"""

    By changing the vertices of the trapezoid, you also define the parameters of the fuzzification:

    a lower limit, x1: $(x1)

    a lower support limit, x2: $(x2)

    an upper support limit, x3: $(x3)

    an upper limit, x4: $(x4)
    """
end

# ╔═╡ ac21300f-dcc8-4637-932b-0c60df5f92ed
md"""
###### x1: $(x1) 

###### x2: $(x2)

###### x3: $(x3)

###### x4: $(x4)


"""

# ╔═╡ 76894458-d3b4-4cfe-abc8-5d40ac786a72
begin
    input_2 = 0:0.1:10
    test_2 = []

    for x in input_2
        out = FuzzyLandscapes.Trapezoidal_MF(x, x1, x2, x3, x4)

        append!(test_2, out)
    end

    plot(input_2, test_2, legend = false, title = "Trapezoidal_MF",
         xlabel = "Fuzzified Parameter, e.g. Distance [km]", ylabel = "Membership Degree")
end

# ╔═╡ 0f738ab6-07ed-4fb6-844f-7d715bd4dfe3
begin
    x11 = @bind x11 Slider(0:0.1:10, default = 2.5)
    x22 = @bind x22 Slider(0:0.1:10, default = 5)
    x33 = @bind x33 Slider(0:0.1:10, default = 7.5)

    md"""
    **Triangular**

    Using the Triangular membership funtion, we could also fuzzify the same parameter, but without the plateau of the Trapeziodial membership function. Here, the membership or suitability increaseas from 2.5 km distance until peaking in 5 km distance and dropping to 0 at 7.5 km distance.

    Thus, by changing the vertices of the triangle, you also define the parameters of the fuzzification:

    a lower limit, x1: $(x11)

    x2, with x1 < x2 < x3: $(x22)

    an upper limit, x3: $(x33)
    """
end

# ╔═╡ 79e16f57-e2e8-4625-bda9-9d4e7c98b73e
md"""
###### x1: $(x11) 

###### x2: $(x22)

###### x3: $(x33)

"""

# ╔═╡ 9833e59a-5b60-46e8-b030-3cdff2d6058a
begin
    input_3 = 0:0.1:10
    test_3 = []

    for x in input_3
        out = FuzzyLandscapes.Triangular_MF(x, x11, x22, x33)

        append!(test_3, out)
    end

    plot(input_3, test_3, legend = false, title = "Triangular_MF",
         xlabel = "Fuzzified Parameter, e.g. Distance [km]", ylabel = "Membership Degree")
end

# ╔═╡ 3c86d8f3-003e-440b-aa2f-3af898df9057
begin
    x111 = @bind x111 Slider(0:0.1:10, default = 2.5)
    x222 = @bind x222 Slider(0:0.1:10, default = 5)

    md"""
    **Z-shaped curve**

    Using the Z-shaped curve, we could also fuzzify the same parameter in more smooth way. Here, distances up to 2.5 km are assigned with a memebership degree of 1. From this point, the membership degree decreses in a curved form, until dropping to 0 at 5 km distance. Thus, x1 and x2  determine the break points of this membership function:

    x1: $(x111)

    x2: $(x222)

    When x2 < x1, Z_MF is a smooth transition from 1 (at x2) to 0 (at x1)

    When x2 >= x1, Z_MF becomes a reverse step function which jumps from 1 to 0
     at (x1+x2)/2

    """
end

# ╔═╡ 43f138dd-faa4-41da-9813-b313c6558d20
md"""
###### x1: $(x111) 

###### x2: $(x222)

"""

# ╔═╡ b88a3942-ffe7-41a6-8a04-f5f97cc16176
begin
    input_4 = 0:0.1:10
    test_4 = []

    for x in input_4
        out = FuzzyLandscapes.Z_MF(x, x111, x222)

        append!(test_4, out)
    end

    plot(input_4, test_4, legend = false, title = "Z_MF",
         xlabel = "Fuzzified Parameter, e.g. Distance [km]", ylabel = "Membership Degree")
end

# ╔═╡ aa8e5975-34af-4c7d-a9ea-ef950fc64dea
md"
# Example 1: Fuzzify a single dataset

Now we can fuzzify real world data. In order to show the capabilities for using raster data as input data, this example contains modified Copernicus Sentinel data (2022).


"

# ╔═╡ addf3e18-2f46-400e-b2cd-86062ce82769
md"""

## Load the data

For this example, we use raster data, containing the slope of an area in Southern-Germany. Slope is defined as the measure of steepness or the degree of inclination of a feature relative to the horizontal plane.

As a first step, we load the data using the package GeoArrays.jl and plot the data using the Plots.jl package:

"""

# ╔═╡ ec11746f-1adb-41f5-b040-2654e6d7841b
slope = "slope.tif"

# ╔═╡ 6c8d305a-603d-4a1c-939e-531420acebe7
slope_gdal = ArchGDAL.readraster(slope)

# ╔═╡ 7c759883-7c10-471c-9ecd-f68eac039eb8
heatmap(slope_gdal[ : , : , 1], colorbar_title = "Slope [°]", cmap = :viridis, clim = (0, maximum(slope_gdal[ : , : , 1])))

# ╔═╡ 889d0796-b771-41fa-b364-f70a0714c554
md"
as we can see from the plot, slope range is from 0 to around 45.
"

# ╔═╡ 3b3f842f-8c68-415e-a3bd-073649acd418
md"

## Fuzzify the slope

As a next step we categorize the slope to three classes using the Trapezoidal membership function method: 
- Flat
- Gentle
- Steep
"

# ╔═╡ b75e337c-d9f0-4dee-a9b2-d3c2967f49bf
begin
    f1 = @bind f1 Slider(0:0.1:45, default = 0)
    f2 = @bind f2 Slider(0:0.1:45, default = 5)

    md"""
    **Define Boundaries of the flat category**

    In this example, we define the values between 0 and 5 as flat. Of course you can change the parameters and see how the fuzzified plot changes:

    the first break point, f1: $(f1)

    the second break point, f2: $(f2)

    """
end

# ╔═╡ 13d608ce-e270-4ed4-a476-b74ecd3ffb9e
md"""
 ###### f1: $(f1)

 ###### f2: $(f2)

 """

# ╔═╡ f60816ae-622f-4225-98e2-8b56a134df46
flat = fuzzy_classification(slope, :Z, f1, f2; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ d9ccf828-fb71-4fa1-a62d-6abb79b3fa8d
heatmap(flat.Fuzzified_Matrix, colorbar_title = "\nMembership Degree", right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ dea1e018-b573-4556-977c-67fcb75aa359
begin
    g1 = @bind g1 Slider(0:0.1:45, default = 5)
    g2 = @bind g2 Slider(0:0.1:45, default = 12.5)

    md"""
    **Define Boundaries of the gentle category**

    Now we define the values between 5 and 12.5 as gentle. Of course you can change the parameters and see how the fuzzified plot changes:

    the first break point, g1: $(g1)

    the second break point, g2: $(g2)

    """
end

# ╔═╡ 92729020-4e8a-4846-b2d4-583bac6325bf
md"""
 ###### g1: $(g1)

 ###### g2: $(g2)

 """

# ╔═╡ 9e1eea5e-2a9e-4940-9233-870716bf24dd
gentle = fuzzy_classification(slope, :Z, g1, g2; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ e2644a2e-2a84-4bdd-82d1-55b76fc6aaab
heatmap(gentle.Fuzzified_Matrix, colorbar_title = "\nMembership Degree", right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ 6272d684-042c-4777-8cd9-b201be199d08
begin
    s1 = @bind s1 Slider(0:0.1:45, default = 12.5)
    s2 = @bind s2 Slider(0:0.1:45, default = 45.0)

    md"""
    **Define Boundaries of the steep category**

    Now we define the values between bigger than 12.5 as steep. Of course you can change the parameters and see how the fuzzified plot changes:

    the first break point, s1: $(s1)

    the second break point, s2: $(s2)

    """
end

# ╔═╡ 90b853e5-8bae-457c-b82e-a0e1106f52e1
md"""
###### s1: $(s1) 
###### s2: $(s2)

"""

# ╔═╡ 2502e38f-9b87-42d5-9f27-94a43b3b0eef
steep = fuzzy_classification(slope, :Z, s1, s2; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ d0711f09-62b1-4210-80fd-02ae46d1f157
heatmap(steep.Fuzzified_Matrix, colorbar_title = "\nMembership Degree", right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ 512d23a4-69b5-4c66-9db5-142757ce7196
begin
    y1 = @bind y1 Slider(0:0.1:50, default = 0)
    y2 = @bind y2 Slider(0:0.1:50, default = 0)
    y3 = @bind y3 Slider(0:0.1:50, default = 10)
    y4 = @bind y4 Slider(0:0.1:50, default = 20)

    md"""


    **Or you can just fuzzify the "whole" slope with the Trapezoidal MF**

    a lower limit, y1: $(y1)

    a lower support limit, y2: $(y2)

    an upper support limit, y3: $(y3)

    an upper limit, y4: $(y4)

    """
end

# ╔═╡ d27e8822-05b0-49dc-ac67-311fe772324a
md"""

###### y1: $(y1) 

###### y2: $(y2)

###### y3: $(y3)

###### y4: $(y4)

"""

# ╔═╡ 13e29b53-e076-4388-b34f-1eb9a20dcb40
tryout = fuzzy_classification(slope, :Trapezoidal, y1, y2, y3, y4; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ 0d46077a-0c6d-4096-97c8-d9788f46ca15
heatmap(tryout.Fuzzified_Matrix, colorbar_title = "\nMembership Degree", right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ 02167084-1d7b-400e-8b5b-16026ec57091
md"

# Example 2: Create a FRBS (Fuzzy Rule Based System)

we could use our classified data and evaluate these in terms of and/or, and perform defuzzification of this data.

For example: if 'flat' or 'gentle', then `class_1`

For this purpose we use FRBS (Fuzzy Rule Based System) function

## Overview of FRBS function:

evalutaion of input datas in terms of and/or, and defuzzification of this data for example, after you classified your data (`FRBS_classification`),
 
=> if 'flat' and/or 'dry', then `class_1`
      'flat' and/or 'dry', then (defuzzy) `class_1`

- Input1 - classified data, for example 'flat'
- Input2 - classified data, for example 'dry'
- Method - choose your method 


Possibilities: 

- :And
- :Or

for further information look into `And_Or.jl`

- `input_matrix` - `source_matrix`, which is used for defuzzyfication,
  for example, if you derived 'flat'/'steep' from 'slope' data, then usually 'slope' 
  data is used as `input_matrix`.
   
if you already used `FRBS_classification` function, then you can use:flat.Source_Matrix

- `Defuzzy_method` - choose your method of defuzzification
                 for further information look into `centroid_bisector_lom_som_mom.jl`

Possibilities: 

- :Centroid
- :Bisector
- :Mom
- :Som
- :Lom
                 

for further information look into `centroid_bisector_lom_som_mom.jl`
"

# ╔═╡ f740770b-49c0-4f73-8d5d-164819f8e672
class_1 = frbs(flat.Fuzzified_Matrix, gentle.Fuzzified_Matrix, :And, flat.Source_Matrix, :Som; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ 2e0f042c-cf72-4a4d-9eff-10ab3a8c820f
heatmap(class_1.Fuzzified_Matrix, colorbar_title = "\nMembership Degree",
        right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ b86710c8-1567-49d8-96c8-d57843076cf6
md"""

# Example 3: Combine terrain parameters with euclidean distance

It can be useful to combine the fuzzified euclidean distance with other fuzzified parameters. For this purpose, a function to calculate the euclidean distance from a specific location is build in the package (`calculate_euclidean_distance`). 

This function requires

- an input dataset, for example the location a .tif file, in this case we use the slope.
-  x: matrix index of the point, from which distances are calculated, along x axis
-  y: matrix index of the point, from  which distances are calculated, along y axis

as input parameters.

The function is also able to calculate the euclidean distance using x and y coordinates. 

"""

# ╔═╡ 8360a9a8-86b5-4057-9024-ac0bac517bcc
md"""

As an Example we use as an input file slope.tif and we want to find the distance from the centre of the map. Or you could define your position for yourself:

"""

# ╔═╡ d6400f42-6a46-4a58-b771-e557cf4fc6c0
begin
    ed1 = @bind ed1 Slider(1:441, default = 223)
    ed2 = @bind ed2 Slider(1:405, default = 202)

end


# ╔═╡ 2d2c8200-f272-40b5-b8ea-4e87fd467094
md"""


 ## Calculate the euclidean distance from a defined cell

    Using the sliders, we can define the origin of the distance raster:

    along x axis: $(ed2)

    along y axis: $(ed1)


    """

# ╔═╡ 4a835c97-3e65-4264-8398-529d427b51bc
md"""
###### x: $(ed2) 

###### y: $(ed1)


"""

# ╔═╡ 19461ee9-3481-47df-a09f-030b0757903a
dist_matrix = calculate_euclidean_distance("slope.tif", ed1, ed2)

# ╔═╡ bd97b51f-7b6f-49d9-a90b-a04160a38265
heatmap(dist_matrix, colorbar_title = "\n\nEuclidean Distance [m]",
        right_margin = 10Plots.mm, cmap = :viridis)

# ╔═╡ db2bd4f9-d6c2-4002-b000-518f744bd2c9
md"""

## Fuzzify the distance

we could classifie this distance into the classes by using membership functions (FRBS_classification function again), for example "near" or "far".

In this case we define "near" as a distance between 0 and a maximum distance in m.

"""

# ╔═╡ d2dfb436-5aa4-4b9d-8aad-190bb08a9654
begin
    ed3 = @bind ed3 Slider(1:20000, default = 10000)

    md"""

    Define the maximum suitable distance: 

    $(ed3)

    """
end

# ╔═╡ da4d91cb-a38f-441e-a3e5-255bdc268a86
md"""

###### Suitable distance [m]: $(ed3)


"""

# ╔═╡ 8d28a161-2603-4bb7-8fff-55b56de96cfc
near = fuzzy_classification(dist_matrix[:, :, 1], :Z, 0, ed3)

# ╔═╡ 94e298ca-e433-41de-8845-87fec84a3a1c
heatmap(near.Fuzzified_Matrix, colorbar_title = "\nMembership Degree", right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ cbcbaf6e-ab24-4731-9f14-2b0103ab9184
md"""

## Create a FRBS

"""

# ╔═╡ e2ee0004-5bfb-44b9-9b22-7439bc1a8263
md"""

Furthermore we could use our classified data ("far" and "gentle") and evaluate these in terms of and/or, and preform defuzzification of this data.

For this purpose we use FRBS (Fuzzy Rule Based System) function, just like above

"""

# ╔═╡ c8b86630-6e3d-460c-a694-e417c896e465
Near_and_flat = frbs(near.Fuzzified_Matrix, flat.Fuzzified_Matrix, :And, flat.Source_Matrix, :Som; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ ec988c9b-6c3d-47a5-950b-ae5589a9ad76
heatmap(Near_and_flat.Fuzzified_Matrix, colorbar_title = "\nMembership Degree",
        right_margin = 5Plots.mm, cmap = :viridis, clim = (0, 1))

# ╔═╡ e0cfd3d0-0dd8-4928-b95a-9b3b9da50b7b
md"""

# Example 4: Export the results

Finally you probaly want to export your data

As an example we export the "near and gentle" data (Example (2))

For this purpose you can use FRBS function as well with additonal arguments
"""

# ╔═╡ d096bf30-adb8-4ca9-8f85-657e2a3becc0
md"""

##### Fuzzy Rule Based System (FRBS), with export

- input1: classified data, for example "near"

- input2: classified data, for example "gentle"

- `input_raster1`: to which raster should the output rearranged if you already used "frbs\_classification" function, then you can use: gentle.Source_Raster

- output: path and name for your output.tif file, e.g. `../derived_data/fuzzy_raster.tif`

- Method: choose your method 

Possible Methods: 

- :And
- :Or

for further information look into `And_Or.jl`

- Input\_matrix - source\_matrix, which is used for defuzzyfication,
  for example, if you derived 'flat'/'steep' from 'slope' data, then usually 'slope' 
  data is used as input_matrix.
   
if you already used 'frbs\_classification' function, then you can use flat.Source_Matrix

- Defuzzy\_method - choose your method of defuzzification
                 for further information look into `centroid_bisector_lom_som_mom.jl`

Possibilities: 

- :Centroid
- :Bisector
- :Mom
- :Som
- :Lom
                 
for further information look into `centroid_bisector_lom_som_mom.jl`
"

"""

# ╔═╡ f65a57d9-e08d-4093-8f89-1c7581a867b2
# FuzzyLandscapes.frbs(near.Fuzzified_Matrix, gentle.Fuzzified_Matrix, "slope.tif", "output.tif", :And, gentle.Source_Matrix, :Som)

# ╔═╡ 770eda92-ea9e-4b31-ad9c-f7d4c40f3af8
# FuzzyLandscapes.frbs(near.Fuzzified_Matrix, gentle.Fuzzified_Matrix, gentle.Source_Raster, "output.tif", :And, gentle.Source_Matrix, :Som)

# ╔═╡ 684bb04c-c4a4-4dde-b8a3-704a1916eaf1
md"""

You can also set the NoData value for your exported raster data 

"""

# ╔═╡ c263db1d-af18-4556-89eb-8e3b182992d1
# FuzzyLandscapes.frbs(near.Fuzzified_Matrix, gentle.Fuzzified_Matrix, "slope.tif", "output.tif", :And, gentle.Source_Matrix, :Som; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ 08f6637c-bb1c-4028-bda3-b20b942ffd9e
# FuzzyLandscapes.frbs(near.Fuzzified_Matrix, gentle.Fuzzified_Matrix, gentle.Source_Raster, "output.tif", :And, gentle.Source_Matrix, :Som; NoData_input = -3.4f38, NoData_output = -3.4f38)

# ╔═╡ a1b447e1-9ef1-4392-9a23-d049c1c80bd8
md"

# References

- L.A. Zadeh, Fuzzy sets, Information and Control, Volume 8, Issue 3, 1965, Pages 338-353, ISSN 0019-9958, https://doi.org/10.1016/S0019-9958(65)90241-X.

"

# ╔═╡ Cell order:
# ╟─fe7d2fc8-0879-432e-b937-0e7c30c30ec6
# ╟─768f6811-3795-434e-9ae1-e72e589b585a
# ╟─f04b7695-7213-4379-b910-262516932041
# ╟─3d290f8e-88d5-4f12-877b-eba0533e6c9b
# ╟─81943deb-eda1-4ef8-8b32-ab79f0a30edf
# ╟─65d7ac9c-b205-4723-942e-6f2d50b69a5e
# ╠═cea81956-b776-4234-a0c6-ea7c8605a282
# ╠═a9e63777-07c5-439e-a210-5333bfb6dbc9
# ╟─72a240f5-63a4-415a-be33-6f7c42893935
# ╟─30b4988c-5fbc-4af4-bd82-36a38c052ca0
# ╟─7532bb6a-bc01-43ac-a9a7-9577515ef114
# ╟─2294d23b-85b4-4920-be4f-7a0deb110e39
# ╟─ad09e1af-6806-496e-b4ba-beb570adac7c
# ╟─ac21300f-dcc8-4637-932b-0c60df5f92ed
# ╟─76894458-d3b4-4cfe-abc8-5d40ac786a72
# ╟─0f738ab6-07ed-4fb6-844f-7d715bd4dfe3
# ╟─79e16f57-e2e8-4625-bda9-9d4e7c98b73e
# ╟─9833e59a-5b60-46e8-b030-3cdff2d6058a
# ╟─3c86d8f3-003e-440b-aa2f-3af898df9057
# ╟─43f138dd-faa4-41da-9813-b313c6558d20
# ╟─b88a3942-ffe7-41a6-8a04-f5f97cc16176
# ╟─aa8e5975-34af-4c7d-a9ea-ef950fc64dea
# ╟─addf3e18-2f46-400e-b2cd-86062ce82769
# ╠═ec11746f-1adb-41f5-b040-2654e6d7841b
# ╠═6c8d305a-603d-4a1c-939e-531420acebe7
# ╟─7c759883-7c10-471c-9ecd-f68eac039eb8
# ╟─889d0796-b771-41fa-b364-f70a0714c554
# ╟─3b3f842f-8c68-415e-a3bd-073649acd418
# ╟─b75e337c-d9f0-4dee-a9b2-d3c2967f49bf
# ╟─13d608ce-e270-4ed4-a476-b74ecd3ffb9e
# ╠═f60816ae-622f-4225-98e2-8b56a134df46
# ╟─d9ccf828-fb71-4fa1-a62d-6abb79b3fa8d
# ╟─dea1e018-b573-4556-977c-67fcb75aa359
# ╟─92729020-4e8a-4846-b2d4-583bac6325bf
# ╠═9e1eea5e-2a9e-4940-9233-870716bf24dd
# ╟─e2644a2e-2a84-4bdd-82d1-55b76fc6aaab
# ╟─6272d684-042c-4777-8cd9-b201be199d08
# ╟─90b853e5-8bae-457c-b82e-a0e1106f52e1
# ╠═2502e38f-9b87-42d5-9f27-94a43b3b0eef
# ╟─d0711f09-62b1-4210-80fd-02ae46d1f157
# ╟─512d23a4-69b5-4c66-9db5-142757ce7196
# ╟─d27e8822-05b0-49dc-ac67-311fe772324a
# ╠═13e29b53-e076-4388-b34f-1eb9a20dcb40
# ╟─0d46077a-0c6d-4096-97c8-d9788f46ca15
# ╟─02167084-1d7b-400e-8b5b-16026ec57091
# ╠═f740770b-49c0-4f73-8d5d-164819f8e672
# ╟─2e0f042c-cf72-4a4d-9eff-10ab3a8c820f
# ╟─b86710c8-1567-49d8-96c8-d57843076cf6
# ╟─2d2c8200-f272-40b5-b8ea-4e87fd467094
# ╟─8360a9a8-86b5-4057-9024-ac0bac517bcc
# ╟─d6400f42-6a46-4a58-b771-e557cf4fc6c0
# ╟─4a835c97-3e65-4264-8398-529d427b51bc
# ╠═19461ee9-3481-47df-a09f-030b0757903a
# ╟─bd97b51f-7b6f-49d9-a90b-a04160a38265
# ╟─db2bd4f9-d6c2-4002-b000-518f744bd2c9
# ╟─d2dfb436-5aa4-4b9d-8aad-190bb08a9654
# ╟─da4d91cb-a38f-441e-a3e5-255bdc268a86
# ╠═8d28a161-2603-4bb7-8fff-55b56de96cfc
# ╟─94e298ca-e433-41de-8845-87fec84a3a1c
# ╟─cbcbaf6e-ab24-4731-9f14-2b0103ab9184
# ╟─e2ee0004-5bfb-44b9-9b22-7439bc1a8263
# ╠═c8b86630-6e3d-460c-a694-e417c896e465
# ╟─ec988c9b-6c3d-47a5-950b-ae5589a9ad76
# ╟─e0cfd3d0-0dd8-4928-b95a-9b3b9da50b7b
# ╟─d096bf30-adb8-4ca9-8f85-657e2a3becc0
# ╠═f65a57d9-e08d-4093-8f89-1c7581a867b2
# ╠═770eda92-ea9e-4b31-ad9c-f7d4c40f3af8
# ╟─684bb04c-c4a4-4dde-b8a3-704a1916eaf1
# ╠═c263db1d-af18-4556-89eb-8e3b182992d1
# ╠═08f6637c-bb1c-4028-bda3-b20b942ffd9e
# ╟─a1b447e1-9ef1-4392-9a23-d049c1c80bd8
