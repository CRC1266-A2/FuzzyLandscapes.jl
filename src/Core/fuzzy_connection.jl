
"""
#  Function of And/Or connection, which also exports the results as .tif file

    input_1 - your input data, for example your location of tif file "../fuzzy_slope.tif"

    input_2 - your input data, for example your location of tif file "../fuzzy_distance.tif"

    output - name of your output file, which be .tif file, e.g. "fuzzy_slope_distance_and.tif"
    
    evaluation_method - choose your method; for further information look into or_and_probOr.jl
                        possibilities:  :And
                                        :Or
                    
    NoData_input - Define the NoData value of the input data

    NoData_output - Define the corresponding NoData value of the output data

# Example

    ```julia-repl

        connection_or = fuzzy_connection("fuzzy_slope.tif", "fuzzy_distance.tif", "fuzzy_slope_distance_or.tif" :Or)

    ```
"""
function fuzzy_connection(input_1::String, input_2::String, output::String, evaluation_method::Symbol; NoData_input::Any = nothing,
    NoData_output::Any = nothing)

    raster_matrix_1, input_raster1 = FuzzyLandscapes.raster_to_matrix(input_1)

    raster_matrix_2, _ = FuzzyLandscapes.raster_to_matrix(input_2)

    intersected_matrix = and_or_evaluation(raster_matrix_1, raster_matrix_2, evaluation_method; NoData_input, NoData_output)

    if !isempty(intersected_matrix)
     
        FuzzyLandscapes.write_raster(intersected_matrix,
                                   input_raster1,
                                   output)

        @info "And/Or successful. Fuzzified raster was written to disk ($output)."

    else
        @error "And/Or unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end


"""
# Function of And/Or connection for two input raster

    input_1 - your input data, for example your location of tif file "../example.tif"
    
    input_2 - your input data, for example your location of tif file "../example.tif"
    
    evaluation_method - choose your method for further information look into or_and_probOr.jl
                        possibilities:  :And
                                        :Or

    NoData_input - Define the NoData value of the input data

    NoData_output - Define the corresponding NoData value of the output data

# Example

    ```julia-repl

        connection_or = fuzzy_connection("fuzzy_slope.tif", "fuzzy_distance.tif", :Or)

    ```
"""
function fuzzy_connection(input_1::String, input_2::String, evaluation_method::Symbol; NoData_input::Any = nothing,
    NoData_output::Any = nothing)

    raster_matrix_1, _ = FuzzyLandscapes.raster_to_matrix(input_1)
    
    raster_matrix_2, _ = FuzzyLandscapes.raster_to_matrix(input_2)

    intersected_matrix = and_or_evaluation(raster_matrix_1, raster_matrix_2, evaluation_method; NoData_input, NoData_output)

    if !isempty(intersected_matrix)

        return intersected_matrix

        @info "And/Or successful."

    else
        @error "And/Or unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end


"""
# Function of And/Or connection for two input matrices

    input_1 - your first input data as a matrix
    
    input_2 - your second input data as a matrix
    
    evaluation_method - choose your method for further information look into or_and_probOr.jl
                        possibilities:  :And
                                        :Or

    NoData_input - Define the NoData value of the input data

    NoData_output - Define the corresponding NoData value of the output data

# Example

    ```julia-repl
    
        connection_or = fuzzy_connection(fuzzy_matrix_1, fuzzy_matrix_2, :Or)

    ```
"""
function fuzzy_connection(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, evaluation_method::Symbol; NoData_input::Any = nothing,
    NoData_output::Any = nothing)

    intersect_output = and_or_evaluation(input_1, input_2, evaluation_method; NoData_input, NoData_output)

    if !isempty(intersect_output)

        return intersect_output

        @info "And/Or successful."

    else
        @error "And/Or unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end

    return intersect_output
end

export fuzzy_connection


## And Or function

"""
# Helper function to evaluate the And/Or connection for two input matrices

    input_1 - your first input data as a matrix
    
    input_2 - your second input data as a matrix

    evaluation_method - choose your method for further information look into or_and_probOr.jl
                        possibilities:  :And
                                        :Or

    NoData_input - Define the NoData value of the input data

    NoData_output - Define the corresponding NoData value of the output data

# Example

    ```julia-repl

        intersect_output = and_or_evaluation(input_1, input_2, :And; NoData_input, NoData_output)

    ```
"""
function and_or_evaluation(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, evaluation_method::Symbol; NoData_input::Any = nothing, NoData_output::Any = nothing)

    intersect_output::Matrix = zeros(Float32, size(input_1))

    if evaluation_method == :And
    
        intersect_output = FuzzyLandscapes.and(input_1, input_2, intersect_output; NoData_input, NoData_output)
    
    elseif evaluation_method == :Or
    
        intersect_output = FuzzyLandscapes.or(input_1, input_2, intersect_output; NoData_input, NoData_output)
    
    end

    return intersect_output
end
