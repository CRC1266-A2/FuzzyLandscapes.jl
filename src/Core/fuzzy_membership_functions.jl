"""
# Trapezoidal membership function type

	Trapezoidal_MF(x, a, b, c, d)

    x: the input data 
    
    vertices of the trapezoid:
        a: a lower limit
        b: a lower support limit
        c: an upper support limit
        d: an upper limit

    for further information: https://de.mathworks.com/help/fuzzy/trapezoidalmf.html

# Example

## Fuzzify a single number

    ```julia-repl
        fuzzy_number = FuzzyLandscapes.Trapezoidal_MF(0.3, 0, .25, .5, .75)
    ```

## Fuzzify a matrix

    ```julia-repl

        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
              y[j] = FuzzyLandscapes.Trapezoidal_MF(x[j], 0, .25, .5, .75)
        end

    ```
"""
function Trapezoidal_MF(x::Real, a::Real, b::Real, c::Real, d::Real; 
                                    NoData_input::Any = nothing, NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    elseif b < c
            if isnan(x)
                x = x
            elseif (a == b)
                a = a - 0.1
            elseif (c == d)
                d = d + 0.1
            end

            out1 = (x > a && x < b) * ((x - a) / (b - a))
            out2 = (x > c && x < d) * ((d - x) / (d - c))
            out3 = (x >= b && x <= c)

            out = out1 + out2 + out3
    else
        @error "Please check your input parameters as the condition 'b < c' is not fulfilled."
    end
end


export Trapezoidal_MF


"""
# Triangular membership function type

     Triangular_MF(x, a, b, c)

	x: the input data
    
    vertices of the triangle:
        a: a lower limit
        b: with a < b < c
        c: an upper limit

      for further information: https://de.mathworks.com/help/fuzzy/trimf.html

# Example

## Fuzzify a single number

    ```julia-repl

        fuzzy_number = FuzzyLandscapes.Triangular_MF(0.3, 0, .25, .5)
    ```
 
 ## Fuzzify a matrix

    ```julia-repl
        x::Matrix = abs.(randn(10,10))
 
        y::Matrix = zeros(10, 10)
 
        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Triangular_MF(x[j], 0, .25, .5)
        end

    ```
"""
function Triangular_MF(x::Real, a::Real, b::Real, c::Real; NoData_input::Any = nothing,
                       NoData_output::Any = nothing)
    if a < b && b < c
        if x == NoData_input || isnan(x)
            out = NoData_output
        else
            if isnan(x)
                out = x
            elseif (x == b)
                out = 1
            else
                out = maximum([minimum([((x - a) / (b - a)), ((c - x) / (c - b))]), 0])
            end
        end
    else
        @error "Please check your input parameters as the condition 'a < b < c' is not fulfilled."
    end
end

export Triangular_MF


"""
# Gaussian membership function type

	Gaussian_MF(x, c, σ)

    x: the input data
    c: the center of the distribution
	σ: determines width of the distribution

    for further information: https://de.mathworks.com/help/fuzzy/gaussmf.html

# Example

## Fuzzify a single number

    ```julia-repl
       
        fuzzy_number = FuzzyLandscapes.Gaussian_MF(0.3, .25, .5)
    
    ```

## Fuzzify a matrix

    ```julia-repl

        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Gaussian_MF(x[j], .25, .5)
        end

    ```
"""
function Gaussian_MF(x::Real, c::Real, σ::Real; NoData_input::Any = nothing,
                     NoData_output::Any = nothing)
    if x == NoData_input  || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            out = exp((-1 / 2) * ((x - c) / σ)^2)
        end
    end
end

export Gaussian_MF


"""
# Two-sided Gaussian membership function type

	Gaussian2_MF(x, c1, σ1, c2, σ2)

    x: the input data
    c1 and σ1 determine the shape of the leftmost curve
	c2 and σ2 determine the shape of the rightmost curve
    Whenever c1 < c2, the Gaussian2_MF function reaches a maximum value of 1. Otherwise, the maximum value is less than one

    for further information: https://de.mathworks.com/help/fuzzy/gauss2mf.html


# Example

## Fuzzify a single number

    ```julia-repl
        fuzzy_number = FuzzyLandscapes.Gaussian2_MF(0.3, .25, .5, 0, .75)
    ```

# Fuzzify a matrix

    ```julia-repl

        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Gaussian2_MF(x[j], .25, .5, 0, .75)
        end
    ```
"""
function Gaussian2_MF(x::Real, c1::Real, σ1::Real, c2::Real, σ2::Real;
                      NoData_input::Any = nothing, NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            c1Index = (x <= c1)
            c2Index = (x >= c2)
            y1 = Gaussian_MF(x, c1, σ1) * c1Index + (1 - c1Index)
            y2 = Gaussian_MF(x, c2, σ2) * c2Index + (1 - c2Index)
            out = y1 * y2
        end
    end
end

export Gaussian2_MF

"""
# Generalised Bell membership function type

	Generalised_bell_MF(x, a, b, c)

    x: is the input data
    a, b and c are the usual bell parameters with c being the center
        Parameter b is usually positive. If b is negative, the shape of this MF becomes an upside-down bell

    for further information: https://de.mathworks.com/help/fuzzy/gbellmf.html

# Example

## Fuzzify a single number
    
    ```julia-repl

        fuzzy_number = FuzzyLandscapes.Generalised_bell_MF(0.3, .25, .5, .75)
    ```

## Fuzzify a matrix

    ```julia-repl
    
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Generalised_bell_MF(x[j], .25, .5, .75)
        end
    ```

"""
function Generalised_bell_MF(x::Real, a::Real, b::Real, c::Real;
                             NoData_input::Any = nothing, NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            out = 1 / (1 + (abs((x - c) / a))^(2 * b))
        end
    end
end

export Generalised_bell_MF



"""
# Sigmoid curve membership function type

	Sigmoid_MF(x, a, c)

    x: the input data
	a: controls slope
	c: is the crossover point
	
    Depending on the sign of the parameter a, a sigmoid MF is inherently open right or left

    for further information: https://de.mathworks.com/help/fuzzy/sigmf.html

# Example

## Fuzzify a single number

    ```julia-repl
    
        fuzzy_number = FuzzyLandscapes.Sigmoid_MF(0.3, .25, .5)
    
    ```

## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Sigmoid_MF(x[j], .25, .5)
        end

    ```
"""

function Sigmoid_MF(x::Real, a::Real, c::Real; NoData_input::Any = nothing,
                    NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            out = 1 / (1 + exp(-a * (x - c)))
        end
    end
end

export Sigmoid_MF


"""
# Product of two sigmoid membership functions type

	p_Sigmoid_MF(x, a1, c1, a2, c2)

    x: the input data
    a1: controls slope of first Sigmoid curve membership function
	c1: is the crossover point of first Sigmoid curve membership function
    a2: controls slope of second Sigmoid curve membership function
	c2: is the crossover point of second Sigmoid curve membership function

    for further information: https://de.mathworks.com/help/fuzzy/psigmf.html

# Example

## Fuzzify a single number

    ```julia-repl

        fuzzy_number = FuzzyLandscapes.p_Sigmoid_MF(0.3, .1, .25, -.5, .75)

    ```

## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.p_Sigmoid_MF(x[j], .1, .25, -.5, .75)
        end
    ```
"""
function p_Sigmoid_MF(x::Real, a1::Real, c1::Real, a2::Real, c2::Real;
                      NoData_input::Any = nothing, NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            sigm_1 = Sigmoid_MF(x, a1, c1)
            sigm_2 = Sigmoid_MF(x, a2, c2)
            out = sigm_1 .* sigm_2
        end
    end
end

export p_Sigmoid_MF


"""
# Difference of two sigmoid membership functions type

	d_Sigmoid_MF(x, a1, c1, a2, c2)

   x: the input data
   a1: controls slope of first Sigmoid curve membership function
   c1: is the crossover point of first Sigmoid curve membership function
   a2: controls slope of second Sigmoid curve membership function
   c2: is the crossover point of second Sigmoid curve membership function

    for further information: https://de.mathworks.com/help/fuzzy/dsigmf.html

# Example

## Fuzzify a single number
    
    ```julia-repl
        
        fuzzy_number = FuzzyLandscapes.d_Sigmoid_MF(0.3, .5, .25, .5, .75)
    
    ```
## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.d_Sigmoid_MF(x[j], .5, .25, .5, .75)
        end
    ```
"""
function d_Sigmoid_MF(x::Real, a1::Real, c1::Real, a2::Real, c2::Real;
                      NoData_input::Any = nothing, NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            sigm_1 = Sigmoid_MF(x, a1, c1)
            sigm_2 = Sigmoid_MF(x, a2, c2)
            out = sigm_1 - sigm_2
        end
    end

    if out < 0
        @error "Please check your input parameters as the fuzzification leads to values below 0."
    else
        return out
    end
end

export d_Sigmoid_MF


"""
# Z-shaped curve membership function type

	Z_MF(x, a, b)

    x is the input data
	a and b  determine the break points of this membership function
    When b < a, Z_MF is a smooth transition from 1 (at b) to 0 (at a)
    When b >= a, Z_MF becomes a reverse step function which jumps from 1 to 0 at (a + b) / 2

    for further information: https://de.mathworks.com/help/fuzzy/zmf.html

# Example

## Fuzzify a single number

    ```julia-repl

        fuzzy_number = FuzzyLandscapes.Z_MF(0.3, .25, .75)

    ```    
## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.Z_MF(x[j], .25, .75)
        end

    ```
"""
function Z_MF(x::Real, a::Real, b::Real; NoData_input::Any = nothing,
              NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        elseif x <= a
            out = 1
        elseif a <= x <= ((a + b) / 2)
            out = 1 - 2 * ((x - a) / (b - a))^2
        elseif ((a + b) / 2) <= x <= b
            out = 2 * ((x - b) / (b - a))^2
        elseif x >= b
            out = 0
        end
    end
end

export Z_MF


"""
# S-shaped curve membership function type

	S_MF(x, a, b)

    x: the input data
	a and b  determines the break points of this membership function
    When a< b, S_MF is a smooth transition from 0 (at a) to 1 (at b)
    When a >= b, S_MF becomes a step function which jumps from 0 to 1 at (a + b)/2

    for further information: https://de.mathworks.com/help/fuzzy/smf.html

# Example

## Fuzzify a single number

    ```julia-repl
        
        fuzzy_number = FuzzyLandscapes.S_MF(0.3, .25, .75)
    ```

## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))

        y::Matrix = zeros(10, 10)

        for j in eachindex(y)
            y[j] = FuzzyLandscapes.S_MF(x[j], .25, .75)
        end
    ```
"""
function S_MF(x::Real, a::Real, b::Real; NoData_input::Any = nothing,
              NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        elseif x <= a
            out = 0
        elseif a <= x <= ((a + b) / 2)
            out = 2 * ((x - a) / (b - a))^2
        elseif ((a + b) / 2) <= x <= b
            out = 1 - 2 * ((x - b) / (b - a))^2
        elseif x >= b
            out = 1
        end
    end
end

export S_MF


"""
# Pi-shaped curve membership function type

	PI_MF(x, a1, b1, a2, b2)

    x: the input data
	a1, b1, a2 and b2 are parameters that determines the break points of this membership function
    The parameters a1 and b2 specify the "feet" of the curve, while b1 and a2 specify its "shoulders"
    This membership function is the product of S_MF and Z_MF
    Note that this Pi MF could be asymmetric because it has four parameters
    This is different from the conventional Pi MF that uses only two parameters

    for further information: https://de.mathworks.com/help/fuzzy/smf.html


# Example

## Fuzzify a single number
   
    ```julia-repl
        fuzzy_number = FuzzyLandscapes.PI_MF(0.3, .1, .25, .5, .75)
    ```

## Fuzzify a matrix

    ```julia-repl
        
        x::Matrix = abs.(randn(10,10))
        
        y::Matrix = zeros(10, 10)
        
        for j in eachindex(y)
            y[j] = FuzzyLandscapes.PI_MF(x[j], .1, .25, .5, .75)
        end
    ```
"""
function PI_MF(x::Real, a1::Real, b1::Real, a2::Real, b2::Real; NoData_input::Any = nothing,
               NoData_output::Any = nothing)
    if x == NoData_input || isnan(x)
        out = NoData_output
    else
        if isnan(x)
            out = x
        else
            sigm_1 = S_MF(x, a1, b1)
            sigm_2 = Z_MF(x, a2, b2)
            out = sigm_1 .* sigm_2
        end
    end
end

export PI_MF
