# FuzzyLandscapes.jl

The goal of this package is the:

- Fuzzification of crisp raster data or 2 dimensional arrays
- Rule-based combination of fuzzy datasets
- Defuzzification of those combined datasets
- Export of the fuzzified datasets

The package uses 2 dimensional arrays as a basis. Thus, you can either fuzzify matrices or datasets read via Rasters.jl. But you can also make use of ArchGDAL datasets or use the path of the raster as an input.

### Installation

To install FuzzyLandscapes.jl from gitlab, open a Julia console and do:

   ```
   julia> using Pkg
   julia> Pkg.add("https://gitlab.com/CRC1266-A2/FuzzyLandscapes.jl.git")

   ```

See the Pluto notebook in the `examples` folder for a basic introduction into fuzzy logic in general and into the basic functionalities of the package.


### Acknowledgements

The package was developed by Ilmar Leimann, Gerrit Günther and Daniel Knitter.

This reposistory is currently not maintained. Development will continue here: https://gitlab.com/GerritGue/FuzzyLandscapes.jl