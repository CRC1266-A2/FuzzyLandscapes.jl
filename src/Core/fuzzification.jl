
"""
# This function uses the input parameters from functions like 'fuzzy_classification()' or 'frbs()' and fuzzifies the input datasets according to these. The function returns a fuzzified matrix.

    fuzzification(input::AbstractMatrix{<:Real}, 
                  fuzzy_matrix::AbstractMatrix{<:Real}, method::Symbol, 
                  x1::Any = nothing, x2::Any = nothing,
                  x3::Any = nothing, x4::Any = nothing; 
                  NoData_input::Any = nothing, NoData_output::Any = nothing)

    input: the input data as a matrix

    fuzzy_matrix: the matrix for the results of the fuzzification

    method: choose your method of membership function for further information look membership_functions.jl. Current possibilities are: :Generalised_bell
                                                        :Trapezoidal
                                                        :Triangular
                                                        :Gaussian
                                                        :Gaussian2
                                                        :Sigmoid
                                                        :D_Sigmoid
                                                        :Z
                                                        :S
                                                        :PI

    x1,x2,x3,x4: parameters of membership function for further information look into membership_functions.jl

    NoData_input: Define a 'NoData_input' value, if you have a numeric 'NoData' in your input data.

    NoData_output: Define a 'NoData_output' value to set the NoData value in the fuzzified set.

# Example

    ```julia-repl

        fuzzified_matrix = fuzzification(input_matrix, fuzzy_matrix, method, x1, x2, x3, x4; NoData_input, NoData_output)

    ```

"""
function fuzzification(input::AbstractMatrix{<:Real}, fuzzy_matrix::AbstractMatrix{<:Real}, method::Symbol, x1::Any = nothing,
                       x2::Any = nothing, x3::Any = nothing, x4::Any = nothing;
                       NoData_input::Any = nothing, NoData_output::Any = nothing)
                       
        
    if method == :Generalised_bell && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Generalised_bell_MF(input[j], x1, x2, x3; NoData_input, NoData_output)
        end
        
    elseif method == :Trapezoidal && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && !isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Trapezoidal_MF(input[j], x1, x2, x3, x4; NoData_input, NoData_output)
        end
        
    elseif method == :Triangular && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Triangular_MF(input[j], x1, x2, x3; NoData_input, NoData_output)
        end
        
    elseif method == :Gaussian && !isnothing(x1) && !isnothing(x2) && isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Gaussian_MF(input[j], x1, x2; NoData_input, NoData_output)
        end
        
    elseif method == :Gaussian2 && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && !isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Gaussian2_MF(input[j], x1, x2, x3, x4; NoData_input, NoData_output)
        end

    elseif method == :Sigmoid && !isnothing(x1) && !isnothing(x2) && isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Sigmoid_MF(input[j], x1, x2; NoData_input, NoData_output)
        end
         
    elseif method == :D_Sigmoid && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && !isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.d_Sigmoid_MF(input[j], x1, x2, x3, x4; NoData_input, NoData_output)
        end

    elseif method == :Z && !isnothing(x1) && !isnothing(x2) && isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.Z_MF(input[j], x1, x2; NoData_input, NoData_output)
        end

    elseif method == :S && !isnothing(x1) && !isnothing(x2) && isnothing(x3) && isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.S_MF(input[j], x1, x2; NoData_input, NoData_output)
        end

    elseif method == :PI && !isnothing(x1) && !isnothing(x2) && !isnothing(x3) && !isnothing(x4)

        for j in eachindex(input)
            fuzzy_matrix[j] = FuzzyLandscapes.PI_MF(input[j], x1, x2, x3, x4; NoData_input, NoData_output)
        end
    end
 
    return fuzzy_matrix
end

### Functions to define the way of fuzzification

"""
# "and" intersection between two parameters and then extracts data from raw data

    Input_1 matrix of parameter 1

    input_2 matrix of parameter 2

    input_3 matrix with raw data
  
    NoData_input: the NoData value of the input datasets
  
    NoData_output: the corresponding NoData value of the output datasets
    
# Example

    ```julia-repl

        out_1 = and_result(flat, dry, twi_data; NoData_input, NoData_output)

    ```
"""
function and_result(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, input_3::Matrix; NoData_input::Any = nothing, NoData_output::Any = nothing)

   boolean_matrix_1::Matrix{eltype(input_1)} = zeros(eltype(input_1), size(input_1))

   boolean_matrix_2::Matrix{eltype(input_2)} = zeros(eltype(input_2), size(input_2))

    for i in eachindex(boolean_matrix_1)
        if boolean_matrix_1[i] == NoData_input
            boolean_matrix_1[i] = NoData_output
        elseif boolean_matrix_2[i] == NoData_input
            boolean_matrix_2[i] = NoData_output
        else
            boolean_matrix_1[i] = boolean(input_1[i])
            boolean_matrix_2[i] = boolean(input_2[i])
        end
    end

    return boolean_to_values(and_boolean(boolean_matrix_1, boolean_matrix_2; NoData_input, NoData_output), 
                                         input_3; NoData_input, NoData_output)
end

export and_result


"""
# Boolean values to real values

	boolean: the input boolean dataSET

    values:  data, from which the values are extracted
  
    NoData_input: the NoData value of the input datasets
  
    NoData_output: the corresponding NoData value of the output datasets
  
# Example

    ```julia-repl

        out_1 = boolean_to_values(boolean, values; NoData_input, NoData_output)

    ```
"""
function boolean_to_values(boolean::AbstractMatrix{<:Real}, values::Matrix; NoData_input::Any = nothing, NoData_output::Any = nothing)

   output::Matrix{eltype(values)} = zeros(eltype(values), size(values))

    for i in eachindex(boolean)
        if boolean[i] == NoData_input
            boolean[i] = NoData_output
        elseif isnan(boolean[i])
            output[i] = NaN
        elseif boolean[i] == 0
            output[i] = 0
        else
            output[i] = values[i]
        end
   end
   return output
end

export boolean_to_values


"""
# Boolean intersection "and" between two data sets

    input_1: the first input dataset as a matrix
    
    input_2: the second input data as a matrix

    NoData_input: the NoData value of the input datasets

    NoData_output: the corresponding NoData value of the output datasets

# Example

    ```julia-repl

        out_1 = and_boolean(input_2, input_10; NoData_input, NoData_output)

    ```
"""
function and_boolean(input_1::AbstractMatrix{<:Real}, input_2::Matrix; NoData_input::Any = nothing, NoData_output::Any = nothing)

   output::Matrix{eltype(input_1)} = zeros(eltype(input_1), size(input_1))

   for i in eachindex(output)
    
        if input_1[i] == NoData_input || input_2[i] == NoData_input
            output[i]= NoData_output
        elseif isnan(input_1[i]) || isnan(input_2[i]) # wenn eine oder ander ist Nan, then nan
               output[i] = NaN
        elseif input_1[i] == 0 || input_2[i] == 0 # wenn eine oder andere ist 0, then output 0
               output[i] = 0
        elseif input_1[i] == 1 && input_2[i] == 1  # wenn eine und andere ist 1, then output 1
               output[i] = 1
        end
    end

   return output
end

export and_boolean

"""
# transform non-boolean values to boolean

    x: an input value

# Example

    ```julia-repl

        out_1 = boolean(input_2)
    ```
"""
function boolean(x::Real)
   if isnan(x)
       x
   elseif x == 0.0 
       0
   else   
       1
   end
end
