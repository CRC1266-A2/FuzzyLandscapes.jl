"""
# Fuzzify an input raster with export

    input: your input data,
            for example your location of tif file "../slope.tif"

    output: path and name for your output .tif file "../derived_data/fuzzy_slope.tif"
             "example" 
             
    method: choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1, x2, x3, x4: parameters of membership function
                     for further information look into membership_functions.jl
    
# Example

    ```julia-repl

        fuzzy_evaluation("slope.tif", "fuzzy_slope.tif", :Z, 0, 5; NoData_input = -3.4f38, NoData_output = -3.4f38)

    ```
"""
function fuzzy_evaluation(input::String, output::String, method::Symbol, x1::Any = nothing,
                    x2::Any = nothing, x3::Any = nothing, x4::Any = nothing;
                    NoData_input::Any = nothing, NoData_output::Any = nothing)

    input_matrix, input_raster = raster_to_matrix(input)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input_matrix), size(input_matrix))

    fuzzified_matrix = fuzzification(input_matrix, fuzzy_matrix, method, x1, x2, x3, x4;
                                    NoData_input, NoData_output)

    if !isempty(fuzzified_matrix)

        FuzzyLandscapes.write_raster(fuzzified_matrix,
                                   input_raster,
                                   output)

        @info "Fuzzification successful. Fuzzified raster was written to disk ($output)."

    else
        @error "Fuzzification unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end


"""
# Fuzzify an input raster without export

    input: your input data,
            for example your location of tif file "../slope.tif"
             
    method: choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1, x2, x3, x4: parameters of membership function
                     for further information look into membership_functions.jl

# Example

    ```julia-repl

        fuzzy_slope = fuzzy_evaluation("slope.tif", :Z, 0, 5; NoData_input = -3.4f38, NoData_output = -3.4f38)

    ```
"""
function fuzzy_evaluation(input::String, method::Symbol, x1::Any = nothing, x2::Any = nothing,
                    x3::Any = nothing, x4::Any = nothing; NoData_input::Any = nothing,
                    NoData_output::Any = nothing)

    input_matrix, _ = raster_to_matrix(input)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input_matrix), size(input_matrix))

    fuzzified_matrix = fuzzification(input_matrix, fuzzy_matrix, method, x1, x2, x3, x4;
                                    NoData_input, NoData_output)

    if !isempty(fuzzified_matrix)

        return fuzzified_matrix

        @info "Fuzzification successful."

    else
        @error "Fuzzification unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end


"""
# Fuzzify and input matrix

    input: input matrix to fuzzify, e.g. a distance matrix
             
    method: choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1, x2, x3, x4: parameters of membership function
                     for further information look into membership_functions.jl

# Example

    ```julia-repl

        fuzzy_slope = fuzzy_evaluation(slope_matrix :Z, 0, 5; NoData_input = -3.4f38, NoData_output = -3.4f38)

    ```
"""
function fuzzy_evaluation(input::AbstractMatrix{<:Real}, method::Symbol, x1::Any = nothing, x2::Any = nothing,
                    x3::Any = nothing, x4::Any = nothing; NoData_input::Any = nothing,
                    NoData_output::Any = nothing)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))

    fuzzified_matrix = fuzzification(input, fuzzy_matrix, method, x1, x2, x3, x4;
                                    NoData_input, NoData_output)

    if !isempty(fuzzified_matrix)

        return fuzzified_matrix

        @info "Fuzzification of matrix successful."
    else
        @error "Fuzzification of matrix unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

export fuzzy_evaluation


"""
# Transform the values of an input raster into a matrix

    input: your input data, for example your location of tif file "../slope.tif"

# Example

    ```julia-repl

        slope:matrix = raster_to_matrix("slope.tif")

    ```
"""
function raster_to_matrix(x::String)

    input_raster::ArchGDAL.RasterDataset{Float32, ArchGDAL.IDataset} = ArchGDAL.readraster(x)

    input_raster_as_vector::Matrix{Float32} = input_raster[:, :, 1]

    return input_raster_as_vector, input_raster
end

export raster_to_matrix
